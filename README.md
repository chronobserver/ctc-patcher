## [CutTheCord][] patcher

### Requires

- `bash`
- `sed`
- `patch`
- `curl`
- `tar`
- `apktool`
- `keytool`
- `jarsigner`
- `adb` (optional if `SKIP_INSTALL` is set)

### Patches

- `bettertm`
- `branding`
- `customfont` ([text][Asap], [code][Hasklig])
- `customversion`
- `necessary`
- `nocamerabutton`
- `noblocked`
- `squareavatars`

### Usage

The script needs a keystore to sign the patched app.

```sh
# the path to a .keystore or .jks file
export KEYSTORE_PATH="ctc-key.jks"
# the alias of the keystore
export KEYSTORE_ALIAS="ctc-key"
```

If the file doesn't exist, generate it with `keytool`.

```sh
keytool -genkeypair -keystore "$KEYSTORE_PATH" -alias "$KEYSTORE_ALIAS"
```

You also need to set the following variable:

```sh
# the version code of the Discord APK
export VERCODE="1340"
```

The variables below are optional:

```sh
# if you don't want to be prompted before patching
export NO_PROMPT=1
# if you want to manually install the patched app
# you will find the APK in com.discord/dist/
export SKIP_INSTALL=1
```

Now, you can run the script like so:

```sh
bash < <(curl -LSs https://discord.coffee/5V4AnGq)
```

You will be prompted to enter the keystore password.

[CutTheCord]: https://gitdab.com/distok/cutthecord
[Asap]: https://fonts.google.com/specimen/Asap
[Hasklig]: https://github.com/i-tu/Hasklig
