#!/usr/bin/env bash

###################################################
# "THE DRINK-WARE LICENSE" (Revision 42):         #
#                                                 #
# As long as you retain this notice you can       #
# do whatever you want with this stuff.           #
# If we meet some day, and you think this stuff   #
# is worth it, you can buy me a drink in return.  #
#                                                 #
# -- Observer of Time <chronobserver@disroot.org> #
###################################################

set -euo pipefail

trap 'exit 130' INT

: "$KEYSTORE_PATH" "$KEYSTORE_ALIAS" "$VERCODE"

printf 'Downloading CutTheCord...\n'

curl --progress-bar -Lf \
  "https://gitdab.com/distok/cutthecord/archive/2021-05-31.tar.gz" | tar xzf -

printf 'Editing patches...\n'

sed -i cutthecord/patches/branding/"$VERCODE".patch \
  -e 's/CTCNAME/Discord (CTC)/g;s/CTCBRANCH/chronobserver/g' \
  -e "s/-CTCBUILD/-$(date --iso-8601)/g;s/CTCBUILD//g"
sed -i cutthecord/patches/branding/customicon.sh -e 's/#rm/rm/'

if [[ -z ${NO_PROMPT+x} ]]; then
  read -rp 'Press enter when you are ready to continue '
fi

for p in necessary nocamerabutton noblocked notrack squareavatars branding customversion; do
  printf 'Applying %s patch...\n' "$p"
  patch --no-backup-if-mismatch -d com.discord -p1 < "cutthecord/patches/$p/$VERCODE.patch"
done

printf 'Replacing files...\n'

pushd com.discord >/dev/null
bash ../cutthecord/patches/branding/customicon.sh
bash ../cutthecord/patches/branding/customdynamicicon.sh
bash ../cutthecord/patches/bettertm/bettertm.sh ../cutthecord/patches/bettertm
popd >/dev/null

curl -LSsfo com.discord/res/font/whitney_semibold.ttf \
  https://github.com/google/fonts/raw/master/ofl/asap/Asap-Medium.ttf
curl -LSsfo com.discord/res/font/whitney_bold.ttf \
  https://github.com/google/fonts/raw/master/ofl/asap/Asap-SemiBold.ttf
curl -LSsfo com.discord/res/font/whitney_medium.ttf \
  https://github.com/google/fonts/raw/master/ofl/asap/Asap-Regular.ttf
curl -LSsfo com.discord/res/font/sourcecodepro_semibold.ttf \
  https://github.com/thiagolucio/HaskligFontFamily-Windows/raw/master/Hasklig-Medium.ttf

apktool b com.discord

while ! jarsigner -keystore "$KEYSTORE_PATH" \
  "com.discord/dist/com.discord.apk" "$KEYSTORE_ALIAS"
do :; done

if [[ -z ${SKIP_INSTALL+x} ]]; then
  adb install -r "com.discord/dist/com.discord.apk"
  rm -rf com.discord/ cutthecord/
fi
